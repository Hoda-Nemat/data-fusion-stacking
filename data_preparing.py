"""
Created on Mar 2020

@author: Hoda Nemat (hoda.nemat@sheffield.ac.uk)

Please cite the following work if you find this code useful for your research:
    
"Hoda Nemat, Heydar Khadem, Jackie Elliott and Mohammed Benaissa,
Data Fusion of Activity and CGM for Predicting Blood Glucose Level,
The 5th International Workshop on Knowledge Discovery in Healthcare Data,
Santiago de Compostela, Spain, August 30, 2020"

"""

import pandas as pd
from pandas import read_csv
from numpy import array
import numpy as np
from datetime import datetime
import pickle as pickle


def FindMissing_interp (seq, subset):
    
    withoutsec_str = [datetime.strptime(str(d), '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d %H:%M') for d in seq.index]
    withoutsec_list = [datetime.strptime(d, '%Y-%m-%d %H:%M') for d in withoutsec_str]
    withoutsec = pd.DatetimeIndex(withoutsec_list)
 
    rounded_withoutsec = [d - pd.Timedelta(minutes=d.minute % 5) for d in withoutsec]
    seq.index = rounded_withoutsec
    seq=seq.loc[~seq.index.duplicated(keep='first')]

    seq_new = pd.Series(seq.values[0], index=[seq.index[0]])


    for i in range(1, len(seq)):
        
        time_delta =  pd.Timestamp(seq.index[i]) - pd.Timestamp(seq.index[i-1])

        if time_delta > pd.Timedelta('9min'):

            seq_nan = seq.reindex(pd.date_range(start=seq.index[i-1],end=seq.index[i], freq='5min', closed='right'))
    
            seq_new=pd.concat([seq_new,seq_nan])
    
        else:
    
            real_row=pd.Series(seq.values[i],index=[seq.index[i]])
            seq_new=pd.concat([seq_new, real_row])
    
    
    nan_mask = np.isnan(seq_new)
    
    if subset == "training":
        interpolated = seq_new.interpolate(limit_direction='both')
    if subset == "testing":
        interpolated = seq_new.interpolate(limit_direction='forward')




    return interpolated, nan_mask

def split_XY (Data, n_steps_in, n_steps_out):
    
    nan_mask = Data['nan_mask']
    del Data['nan_mask']
    Data_array = array(Data)

    
    X = []
    Y = []
    X_NanMask = []
    Y_NanMask = []
    Univar = []

    
    for i in range(len(Data_array)):
        
        # find the end of this pattern
        end_ix = i + n_steps_in
        out_end_ix = end_ix + n_steps_out
        # check if we are beyond the sequence
        if out_end_ix > len(Data_array):
            break
        
        seq_x = Data_array[i:end_ix] # To use for the creation of multivar array 
        seq_y = Data['BG'][end_ix:out_end_ix]
        
        X_withNan = nan_mask[i:end_ix]
        Y_withNan = nan_mask[end_ix:out_end_ix]
    

        if Y_withNan[-1] == False:
           
           
            X.append(seq_x)
            Y.append(seq_y)
            
            
            X_NanMask.append(X_withNan)
            Y_NanMask.append(Y_withNan)
            
            Univar_row = []
        
            ########### creating univariate ###############################
            
            Univar_row = list(seq_x[:,0]) #BG
            Univar_row.append(np.mean(seq_x[:,1])) #acceleration
            Univar.append(Univar_row)

        
    XNanMask_array = array(X_NanMask)       
    YNanMask_array = array(Y_NanMask) 
       
    Xunivar_array = array(Univar)
    X_array = array(X)       
    Y_array = array(Y)
    
    return X_array, Xunivar_array, Y_array, XNanMask_array, YNanMask_array




def FindMissing_activity (Data, seq_acceleration, subset, Avg_acc):

    seq = seq_acceleration
    withoutsec_str = [datetime.strptime(str(d), '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d %H:%M') for d in seq.index]
    withoutsec_list = [datetime.strptime(d, '%Y-%m-%d %H:%M') for d in withoutsec_str]
    withoutsec = pd.DatetimeIndex(withoutsec_list)
    seq.index = withoutsec
    seq=seq.loc[~seq.index.duplicated(keep='first')]

    seq_new = pd.Series(seq.values[0],index=[seq.index[0]])
  
    for i in range(1,len(seq)):
        
        time_delta =  pd.Timestamp(seq.index[i]) - pd.Timestamp(seq.index[i-1])
        if time_delta > pd.Timedelta('110S'):

            seq_nan = seq.reindex(pd.date_range(start=seq.index[i-1], end=seq.index[i], freq='1min', closed='right')) 
            seq_new=pd.concat([seq_new,seq_nan])
    
        else:
    
            real_row=pd.Series(seq.values[i], index=[seq.index[i]])
            seq_new=pd.concat([seq_new, real_row])
            
    seq_new = seq_new.replace(0, np.nan)
    
        
    if subset == "training":
        activity_interpolated = seq_new.interpolate(limit_direction='both')
        
    if subset == "testing":
        activity_interpolated = seq_new.interpolate(limit_direction='forward')    
    

    activity_seq = pd.DataFrame({'activity':float(Avg_acc)}, index=Data.index)      
    
    activity_interp = pd.DataFrame({'acc_interp':activity_interpolated.values}, index=seq_new.index)
    sync_index = activity_interp.index.intersection(Data.index)
    activity_sync = activity_interp.loc[sync_index]
    
    activity_seq.loc[sync_index] = activity_sync
        
    return activity_seq

def creating_DF (PID):
    
    d = {}
    subset=['training','testing']
    data_type_list = ['glucose_value','acceleration']

    
    for k in range(len(subset)):
        
        d[subset[k]] = [data_type + '_' + PID + '_' + subset[k] + '.csv' for data_type in data_type_list]

        seq_BG = read_csv(d[subset[k]][0], header=0, index_col=0, parse_dates=True, squeeze=True)
        seq_glucose_value, nan_mask = FindMissing_interp(seq_BG, subset=subset[k])
        BG = seq_glucose_value.values
        TS_BG = seq_glucose_value.index
        Data = pd.DataFrame({'BG': BG, 'nan_mask': nan_mask}, index=TS_BG)
        
        
        ## activity
        seq_acceleration = read_csv(d[subset[k]][1], header=0, index_col=0, parse_dates=True, squeeze=True)
        
        # Inserting the average of acceleration in training set for missing points        
        d['Avg_acc_{}'.format(subset[k])] = np.sum(seq_acceleration)/np.count_nonzero(seq_acceleration) 

        activity_seq_interp = FindMissing_activity(Data, seq_acceleration, subset=subset[k], Avg_acc=d['Avg_acc_training'])
        Data['acceleration'] = activity_seq_interp['activity']
        
        
        d['Data_{}'.format(subset[k])] = Data[-11:]
        
        if k == 0:
        
            Data_6_6 = Data[6:]
            Data_6_12 = Data[6:]
            Data_12_6 = Data[:]
            Data_12_12 = Data[:]

        if k == 1:

            frames = [d['Data_training'], Data]
            Data = pd.concat(frames)
            
            Data_6_6 = Data[12:]
            Data_6_12 = Data[6:]
            Data_12_6 = Data[6:]
            Data_12_12 = Data[:]

        #######################################################################
        Data.to_csv('%s_%s.csv' % (subset[k], PID))

        
        
        d['X_{}_6_6'.format(subset[k])],d['Xunivar_{}_6_6'.format(subset[k])], d['Y_{}_6_6'.format(subset[k])], d['XNanMask_{}_6_6'.format(subset[k])], d['YNanMask_{}_6_6'.format(subset[k])] = split_XY(Data_6_6, n_steps_in=6, n_steps_out=6)
        d['X_{}_6_12'.format(subset[k])],d['Xunivar_{}_6_12'.format(subset[k])], d['Y_{}_6_12'.format(subset[k])], d['XNanMask_{}_6_12'.format(subset[k])], d['YNanMask_{}_6_12'.format(subset[k])] = split_XY(Data_6_12, n_steps_in=6, n_steps_out=12)
        d['X_{}_12_6'.format(subset[k])],d['Xunivar_{}_12_6'.format(subset[k])], d['Y_{}_12_6'.format(subset[k])], d['XNanMask_{}_12_6'.format(subset[k])], d['YNanMask_{}_12_6'.format(subset[k])] = split_XY(Data_12_6, n_steps_in=12, n_steps_out=6)
        d['X_{}_12_12'.format(subset[k])],d['Xunivar_{}_12_12'.format(subset[k])], d['Y_{}_12_12'.format(subset[k])], d['XNanMask_{}_12_12'.format(subset[k])], d['YNanMask_{}_12_12'.format(subset[k])] = split_XY(Data_12_12, n_steps_in=12, n_steps_out=12)
    
    
    TrainTestXY_6_6 = (d['X_training_6_6'], d['Y_training_6_6'], d['X_testing_6_6'], d['Y_testing_6_6'], d['XNanMask_training_6_6'], d['YNanMask_training_6_6'], d['XNanMask_testing_6_6'], d['YNanMask_testing_6_6'])
    TrainTestXY_univar_6_6 = (d['Xunivar_training_6_6'], d['Y_training_6_6'], d['Xunivar_testing_6_6'], d['Y_testing_6_6'], d['XNanMask_training_6_6'], d['YNanMask_training_6_6'], d['XNanMask_testing_6_6'], d['YNanMask_testing_6_6'])
    
    TrainTestXY_6_12 = (d['X_training_6_12'], d['Y_training_6_12'], d['X_testing_6_12'], d['Y_testing_6_12'], d['XNanMask_training_6_12'], d['YNanMask_training_6_12'], d['XNanMask_testing_6_12'], d['YNanMask_testing_6_12'])
    TrainTestXY_univar_6_12 = (d['Xunivar_training_6_12'], d['Y_training_6_12'], d['Xunivar_testing_6_12'], d['Y_testing_6_12'], d['XNanMask_training_6_12'], d['YNanMask_training_6_12'], d['XNanMask_testing_6_12'], d['YNanMask_testing_6_12'])
    
    TrainTestXY_12_6 = (d['X_training_12_6'], d['Y_training_12_6'], d['X_testing_12_6'], d['Y_testing_12_6'], d['XNanMask_training_12_6'], d['YNanMask_training_12_6'], d['XNanMask_testing_12_6'], d['YNanMask_testing_12_6'])
    TrainTestXY_univar_12_6 = (d['Xunivar_training_12_6'], d['Y_training_12_6'], d['Xunivar_testing_12_6'], d['Y_testing_12_6'], d['XNanMask_training_12_6'], d['YNanMask_training_12_6'], d['XNanMask_testing_12_6'], d['YNanMask_testing_12_6'])
    
    TrainTestXY_12_12 = (d['X_training_12_12'], d['Y_training_12_12'], d['X_testing_12_12'], d['Y_testing_12_12'], d['XNanMask_training_12_12'], d['YNanMask_training_12_12'], d['XNanMask_testing_12_12'], d['YNanMask_testing_12_12'])
    TrainTestXY_univar_12_12 = (d['Xunivar_training_12_12'], d['Y_training_12_12'], d['Xunivar_testing_12_12'], d['Y_testing_12_12'], d['XNanMask_training_12_12'], d['YNanMask_training_12_12'], d['XNanMask_testing_12_12'], d['YNanMask_testing_12_12'])
    
   
    with open('TrainTestXY_%s_history6_horizon6.pkl'%(PID), 'wb') as f:
        pickle.dump(TrainTestXY_6_6, f, protocol=2)
        
    with open('TrainTestXY_%s_history6_horizon12.pkl'%(PID), 'wb') as f:
        pickle.dump(TrainTestXY_6_12, f, protocol=2)    
        
    with open('TrainTestXY_%s_history12_horizon6.pkl'%(PID), 'wb') as f:
        pickle.dump(TrainTestXY_12_6, f, protocol=2)

    with open('TrainTestXY_%s_history12_horizon12.pkl'%(PID), 'wb') as f:
        pickle.dump(TrainTestXY_12_12, f, protocol=2)  
        
        
        
    with open('TrainTestXY_univar_%s_history6_horizon6.pkl'%(PID), 'wb') as f:
        pickle.dump(TrainTestXY_univar_6_6, f, protocol=2)
        
    with open('TrainTestXY_univar_%s_history6_horizon12.pkl'%(PID), 'wb') as f:
        pickle.dump(TrainTestXY_univar_6_12, f, protocol=2)
        
    with open('TrainTestXY_univar_%s_history12_horizon6.pkl'%(PID), 'wb') as f:
        pickle.dump(TrainTestXY_univar_12_6, f, protocol=2)
        
    with open('TrainTestXY_univar_%s_history12_horizon12.pkl'%(PID), 'wb') as f:
        pickle.dump(TrainTestXY_univar_12_12, f, protocol=2)  
        

    ###########################################################################



creating_DF(PID = '540')

    
    
    
    
    
    
    