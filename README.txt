Citation:
Please cite this work if you find this repository useful for your research:
Nemat, Hoda, et al. "Data fusion of activity and CGM for predicting blood glucose levels." Knowledge Discovery in Healthcare Data 2020. Vol. 2675. CEUR Workshop Proceedings, 2020.
https://eprints.whiterose.ac.uk/166816/

Requirements:
The codes require Python ≥ 3.6, TensorFlow ≥ 1.15.0, Keras ≥ 2.2.5, Pandas, NumPy and Sklearn. 

Usage:
Access to the Ohio dataset is required to reproduce the results.
Run the 'xml2csv.py' file to extract blood glucose and acceleration data from XML files and save them in CSV files. Note that the XML path needs is the path on disk where the to the folder containing the XML files for the Ohio dataset.
Run 'data_preparing.py' to take care of missing data, and translating the time series problem to a supervised learning task.
Run 'methods.py' to implement the prediction methods. RMSE, MAE and the raw results will be automatically saved.
