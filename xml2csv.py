"""
Created on Mar 2020

@author: Hoda Nemat (hoda.nemat@sheffield.ac.uk)

Please cite the following work if you find this code useful for your research:
    
"Hoda Nemat, Heydar Khadem, Jackie Elliott and Mohammed Benaissa,
Data Fusion of Activity and CGM for Predicting Blood Glucose Level,
The 5th International Workshop on Knowledge Discovery in Healthcare Data,
Santiago de Compostela, Spain, August 30, 2020"

"""


import os
import xml.etree.ElementTree as ET
import pandas as pd
from datetime import datetime



def XMLtoCSV (data_type):

    d = {}
    
    subset_list=['training','testing']
    
    for k in range(len(subset_list)):
        Pid_list = ['540', '544', '552', '567', '584', '596']
        d[subset_list[k]] = ['C:/Users/'+ os.getenv('username') +'/Ohio/Dataset/OhioT1DM-2-' + subset_list[k] + '/'+ pID + '-ws-' + subset_list[k] + '.xml' for pID in Pid_list]
        
        for i in range(len(Pid_list)):
            tree = ET.parse(d[subset_list[k]][i])
            root = tree.getroot() 
            if data_type == 'glucose_value':
                Data_type_index = 0
                
            if data_type == 'acceleration':
                if i == 5:
                    if k == 0:
                        Data_type_index = 16
                    if k == 1:
                        Data_type_index = 17
                else:
                    Data_type_index = 18

            Glucose = root[Data_type_index]
            GlucoseChildren = Glucose.getchildren()
                
            TS = []
            BGV = []
            
            for j in range(len(GlucoseChildren)):
                r = Glucose.getchildren()[j]
                ts = r.attrib['ts']
                TS.append(ts)
                value = r.attrib['value']
                BGV.append(value)
                
            TS_new = [datetime.strptime(d, '%d-%m-%Y %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S') for d in TS]
            Data = pd.Series(BGV, index=TS_new)
            Data.to_csv("%s_%s_%s.csv" % (data_type, Pid_list[i], subset_list[k]))  
            
XMLtoCSV (data_type='glucose_value')            
XMLtoCSV (data_type='acceleration')